// credit:  
// https://github.com/jepusto/jepusto.com/blob/main/static/js/codefolding.js
window.initializeCodeFolding = function(show) {

  // index for unique code element ids
  var currentIndex = 1;

  // select all R code blocks
  var rCodeBlocks = $('pre.sourceCode, pre.downlit, pre.r, pre.python, pre.bash, pre.sql, pre.cpp, pre.stan, pre.js');

  rCodeBlocks.each(function() {

    var div = $('<div class=collapse></div>');

    var id = 'collapseExample' + currentIndex++;

    div.attr('id', id);

    $(this).before(div);

    $(this).detach().appendTo(div);

    // add a show code button right above
    var showCodeText = $('<span>' + (show ? 'Hide' : 'Code') + '</span>');
    var showCodeButton = $('<button type="button" class="btn btn-default btn-xs code-folding-btn pull-right"></button>');

    showCodeButton.append(showCodeText);
    showCodeButton
        .attr('data-toggle', 'collapse')
        .attr('data-target', '#' + id)
        .attr('aria-expanded', show)
        .attr('aria-controls', id);

    var buttonRow = $('<div class="row"></div>');
    var buttonCol = $('<div class="col-md-12"></div>');

    buttonCol.append(showCodeButton);
    buttonRow.append(buttonCol);

    div.before(buttonRow);

    div.on('hidden.bs.collapse', function () {
      showCodeText.text('Code');
    });

    div.on('show.bs.collapse', function () {
     showCodeText.text('Hide');
    });

  });

}
