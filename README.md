# Repository for the Manual of R-package IdiogramFISH

This manual was built with [bookdown](https://bookdown.org/yihui/bookdown/), an R-package

Open manual: <https://ferroao.gitlab.io/manualidiogramfish/>

idiogramFISH R-package Repo: <https://gitlab.com/ferroao/idiogramfish>

### Source

This manual is mainly based on the idiogramFISH package online vignette

* `refs/` folder:
https://gitlab.com/ferroao/idiogramFISH/-/tree/master/vignettes/refs?ref_type=heads

* `index.Rmd` and chapter 16:
https://gitlab.com/ferroao/idiogramFISH/-/tree/master/vignettes?ref_type=heads

* chapters 01 and 02: https://gitlab.com/ferroao/idiogramFISH/-/tree/master/parts?ref_type=heads
* other `.Rmd` files: https://gitlab.com/ferroao/idiogramFISH/-/tree/master/chapters?ref_type=heads
* `js/` file: https://github.com/jepusto/jepusto.com/blob/main/static/js/codefolding.js

* `figures/` folder files: https://gitlab.com/ferroao/idiogramFISH/-/tree/master/onlinefigures?ref_type=heads
https://gitlab.com/ferroao/idiogramFISH/-/tree/master/man/figures?ref_type=heads

### Render:

* Check pandoc: `rmarkdown::pandoc_exec()`
* Define pandoc in `PATH` variable, if missing, as in `.Rprofile`
* In R run: `bookdown::render_book("index.Rmd", "all")`