# Datasets

## Chr. basic data Holo.
 
`dfChrSizeHolo`: Example data for holocentrics for 1 species

`bigdfChrSizeHolo`: Example data for holocentrics for several species, OTU

`parentalAndHybHoloChrSize`: Example data for holocentrics for several species, OTU

`bigdfOfChrSize3Mb`: Example data in Mb without chr. arms for three species, OTU

### Format {-}

data.frame with columns:

`OTU` grouping OTU (species), optional if only one OTU

`chrName` name of chromosome

`chrSize` size of chromosome, micrometers or Mb

`group` chromosome group, optional

`chrNameUp` optional name over kar.

`Mbp` optional name to show size in Mbp under kar., use only when chrSize is not in Mbp. To be used with `chrSizeMbp = TRUE`

## Chr. basic data Monocen.

`dfOfChrSize`: Example data for monocentrics

`bigdfOfChrSize`: Example data for monocentrics for several species, OTU

`humChr`: Data for human karyotype, measured from Adler (1994)

`allChrSizeSample`: Example data for monocentrics for several species, OTU

`parentalAndHybChrSize`: Example data for monocentrics for GISH

`traspadf`: Example data for Tradescantia (Rhoeo) spathacea [@Golczyk2005]

### Format {-}

data.frame with columns:

`OTU` species, optional if only one OTU (species)

`chrName` name of chromosome

`shortArmSize` size of short arm, micrometers

`longArmSize` size of long arm, micrometers

`group` chr group, optional

`chrNameUp` optional name over kar.

`Mbp` optional name to show size in Mbp, use only when shortArmSize is not in Mbp


## Mark characteristics

Source: R/dfMarkStyle.R

`style` column does not apply to cen. marks, only color.

`dfMarkColor`: Example General data for marks NOT position

`humMarkColor`: human bands' characteristics, from Adler [-@Adler1994]

`mydfMaColor`: mark characteristics used in vignette of phylogeny

### Format {-}

dfMarkColor a data.frame with columns:

`markName` name of mark

`markColor` use R colors

`style` character, use square or dots, optional

`protruding` numeric, modifies aspect of cM/cMLeft marks, see parameter protruding in plotIdiograms, optional


## Mark Positional data - Holocen.

Source: R/markdataholo.R

`bigdfMarkPosHolo`: Example data for mark position of holocentrics with column OTU

`dfMarkPosHolo`: Example data for mark position of holocentrics

`dfAlloParentMarksHolo`: Example data for mark position of GISH

`bigdfOfMarks3Mb`: Example data for mark position in Mb

### Format {-}

data.frame with columns:

`OTU` OTU, species, optional

`chrName` name of chromosome

`markName` name of mark

`markPos` position from bottom or top (see parameter origin in plotIdiograms)

`markSize` size of mark in micrometers or Mb

## Mark Positional data - monocentrics

Source: R/markposDFs.R

`bigdfOfMarks`: Example data for mark position with column OTU

`dfOfMarks`: Example data for marks' position

`dfOfMarks2`: Marks' position including cen. marks

`humMarkPos`: human karyotype bands' (marks) positions, measured from Adler (1994)

`allMarksSample`: Example data for marks' position

`dfAlloParentMarks`: Example data for mark position of GISH of monocen.

`traspaMarks`: *T. spathacea* (Rhoeo) marks' positions, from Golczyk et al. [-@Golczyk2005]

### Format {-}

bigdfOfMarks a data.frame with columns:

`OTU` OTU, species, mandatory if in dfChrSize

`chrName` name of chromosome

`markName` name of mark

`chrRegion` use p for short arm, q for long arm, and cen for centromeric

`markDistCen` distance of mark to centromere (not for cen)

`markSize` size of mark (not for cen)

## File `eightSpIqtree.treefile`

See chapter phylogeny [@Nguyen2015c]

## File `revBayesTutorial.tree`

See chapter phylogeny [@Hohna2017]

